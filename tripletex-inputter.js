var new_row = function( args ) {
  $('#ajaxContenttoolbarContainer #newRowButton').click();
  //$('#newRowButton:eq(0) span').click();

  var T = 100;

  // Register the row
  setTimeout( function() {
    if ( undefined === rows[args.project] )
      rows[args.project] = {};
    rows[args.project][args.type] = $('.hoursRow:eq(0)');
  }, T );

  if ( args.type !== '' ) {
    // Click project select
    setTimeout( function() {
      $('[id$="projectIdSelect"]:eq(0)').click();
    }, T );
    T += 2900;
    setTimeout( function() {
      // Select project
      var projects = $( '.tlxSelectListDiv .tableContent:nth-of-type(1) :first-child' );
      projects.filter(function(){
        return $(this).text() == args.project;
      } ).click();
    }, T );
    T += 1000;
  }

  // Click activity select
  setTimeout( function() {
    $('[id$="activityIdSelect"]:eq(0)').click();
  }, T );
  T += 1000;
  setTimeout( function() {
    // Select activity type
    var activities = $( '.tlxSelectListDiv .tableContent:nth-of-type(2)' );
    var activity = activities.filter(function(){
      return $(this).text() == ( args.type ? args.type : args.project );
    } ).click();
    setTimeout( function() {
      activity.click();
    }, 500);
  }, T );
};

var rows = {};

// Hide clock
var offset = 1;
if ( $('[id^="ajaxContentrow"]:not(.commentRow) td.do:nth-child(1)').length ) {
  offset = 2;
}

// Register all existing rows
$('[id^="ajaxContentrow"]:not(.commentRow) td:nth-of-type('+ offset +')').each( function() {
  var cell = $( this );
  // Trim project and type names
  var project = $.trim( cell.text() );
  var type = $.trim( cell.next().text() );
  if ( ! project ) {
    // Tripletext has placed special projects in the aktivity column
    // Switch them around to match the old style
    project = type;
    type = "";
  }
  // Remove dots from project name
  project = project.replace( /^\.\s+\.\s+/, '' );

  // console.log( "Project: " + project );
  // console.log( "Type: " + type );
  // return;
  if ( undefined === rows[project] )
    rows[project] = {};
  rows[project][type] = cell.parent();
} );

var set = function( args ) {
  // Check for row
  if ( undefined === rows[args.project] || undefined === rows[args.project][args.type] ) {
    new_row( args );
    // Re-schedule this function
    setTimeout( function() { set( args ); }, 6000 );
    return;
  }
  var row = rows[args.project][args.type];
  var hour_input = row.find('.hours:eq('+ args.day +')');
  // Trigger the hour input
  hour_input.click().focus().trigger('focus');

  setTimeout( function() {
    // Trigger the hour input again for safety
    hour_input.click().focus().trigger('focus');
    hour_input.val( args.hours );
    // Trigger the description textarea
    var desc_input = row.next().find( 'textarea' );
    desc_input.click().val( args.description ).trigger( 'blur' );
  }, 500 );

  if ( args.callback )
    setTimeout( args.callback, 500 );
};
var my_hours = [];

var input_has_value = function() {
  return $( this ).val() !== '';
};

var clear_empty_rows = function() {
  // var r = $();
  // $( '.hourlistSum' ).filter( function() {
  //   if ( '0,0' != $( this ).text() )
  //     return false;
  //   var inputs = $( this ).parent().find( 'input.hours' ).filter( input_has_value );
  //   console.log( inputs.length );
  //   if ( inputs.length )
  //     return false;
  //   return true;
  // } ).each( function() {
  //   r = r.add( $( this ).parent().parent() );
  // } );
  // r.filter( '[id]' ).find( 'td:eq(0) button' ).click();
};

var run_queue = function( first_run  ) {
  // var week = $('#ajaxContentperiod').val().match(/\w+ (\d+) \d+/)[1];
  var week = $('.tlx-title:first').text().match(/\(Uke (\d+),/)[1];

  if ( first_run ) {
    console.log( '%c Running queue for week '+week, 'color: #C00; font-size: 1.5rem; font-weight: 600' );
  }

  if ( my_hours[week] && my_hours[week].length ) {
    var current_hours = my_hours[week].pop();
    current_hours.callback = run_queue;
    console.log( 'Logging for: '+ current_hours.project +' - '+ current_hours.type );
    set( current_hours );
  }
  else {
    clear_empty_rows();
    console.log( 'All done' );
  }
};
