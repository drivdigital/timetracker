( function( w ) {

  var date;
  var context_awareness = function( context, elem ) {
    var lines = context.content.substr( 0, context.ls ).split( "\n");
    for ( var i = lines.length - 1; i >= 0; i-- ) {
      var line = lines[i];
      if ( ! line.match( /^\d{4}\-\d{2}\-\d{2}$/ ) ) {
        continue;
      }
      if ( date != line ) {
        $( document ).trigger( 'new_date', [ line, date ] );
      }
      date = line;
      break;
    }
  }
  $( document ).on( 'new_date', function( event, new_date ) {
    $.get( '/context.php?date=' + new_date, function( data ) {
      $( '.context' ).html( data );
    } );
  } );

  w.context_awareness = context_awareness;
} ( window ) );
