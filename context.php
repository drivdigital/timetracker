<?php
date_default_timezone_set( 'Europe/Oslo' );

$date = @$_GET['date'];

echo "<h2>$date</h2>";

$time = strtotime( $date );

$year = date( 'Y', $time );
$week = date( 'W', $time );
$month = date( 'M', $time );
$day = date( 'd', $time );

$folder = "file-checker/$year/W$week/$month-$day";

if ( ! file_exists( $folder ) ) {
  echo "<p>No info for $folder</p>";
  die;
}

$files = glob( "$folder/*.json" );

echo "<pre>";
foreach ( $files as $file ) {
  $content = json_decode( file_get_contents( $file ), 1 );
  foreach ($content as $time => $file) {
    echo date('H:i',$time) ." $file\n";
  }
}
echo "</pre>";


